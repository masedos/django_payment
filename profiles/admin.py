from django.contrib import admin
from profiles.models import Profile, UserStripe

# Register your models here.

class ProfileAdmin(admin.ModelAdmin):
     class Meta:
         model = Profile
        
admin.site.register(Profile)


class UserStripeAdmin(admin.ModelAdmin):
     class Meta:
         model = UserStripe
        
admin.site.register(UserStripe)
