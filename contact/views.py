from django.shortcuts import render
from django.http import Http404
from django.conf import settings

from django.core.mail import send_mail

from contact.forms import ContactForm

# Create your views here.

def contact(request):
    title = "Contact"
    confirm_message = None
    
    form = ContactForm(request.POST or None)
    if form.is_valid():
        comment = form.cleaned_data['comment']
        name = form.cleaned_data['name']
        subject = 'Message from Cloud9'
        message = '%s %s' %(comment, name)
        email_from  = form.cleaned_data['email']
        email_to  = [settings.EMAIL_HOST_USER]
        send_mail(subject, message, email_from, email_to, fail_silently=True)
        title = "Thanks"
        confirm_message = "Thansk for the message!"
        form = None
        
    context = {
        'title': title,
        'form': form,
        'confirm_message': confirm_message,
        }
    template = 'contact.html'
    return render(request, template, context)
